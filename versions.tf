terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.23.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.2.0"

    }
  }

  backend "http" {
  }

  required_version = ">= 1.1"
}

provider "google" {
  project = var.project
  region  = var.location
}
